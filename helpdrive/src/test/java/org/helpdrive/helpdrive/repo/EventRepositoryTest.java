package org.helpdrive.helpdrive.repo;

import org.hamcrest.Matchers;
import org.helpdrive.helpdrive.model.Category;
import org.helpdrive.helpdrive.model.Event;
import org.helpdrive.helpdrive.model.Status;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Metrics;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created by skipy on 07.04.18.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class EventRepositoryTest {

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Test
    @Ignore // unignored if test it ;)
    public void findByCategoryAndLocationNear() throws Exception {
        Iterable<Category> categoryes = Arrays.asList(newCategory("title1", "test1"), newCategory("title2", "test2"));
        categoryes = categoryRepository.insert(categoryes);
        Category category = categoryes.iterator().next();
        Event event = eventRepository.insert(newEvent(category));

        List<Event> lst = eventRepository.findEventsByCategoryAndLocationNear(category, new Point(13.205838, 52.531261),
                new Distance(1000., Metrics.MILES));
        Assert.assertThat(lst, Matchers.not(Matchers.emptyCollectionOf(Event.class)));

        Iterator<Category> iterator = categoryes.iterator();
        iterator.next();
        lst = eventRepository.findEventsByCategoryAndLocationNear(iterator.next(), new Point(13.205838, 52.531261),
                new Distance(1000., Metrics.MILES));

        Assert.assertThat(lst, Matchers.emptyCollectionOf(Event.class));
    }

    private Event newEvent(Category category) {
        Event event = new Event();
        event.setCategory(category);
        event.setStatus(Status.OPEN);
        event.setLocation(new GeoJsonPoint(13.405838, 52.531261));
        event.setUserExecutor(Collections.emptySet());
        event.setDescription("test");
        event.setPrice(BigDecimal.TEN);
        event.setId(String.valueOf(System.currentTimeMillis()));
        return event;
    }

    private Category newCategory(String title, String desc) {
        Category category = new Category();
        category.setTitle(title);
        category.setDescription(desc);
        category.setHashId();
        return category;
    }
}