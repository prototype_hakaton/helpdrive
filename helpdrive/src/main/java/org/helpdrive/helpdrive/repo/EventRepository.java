package org.helpdrive.helpdrive.repo;

import org.helpdrive.helpdrive.model.Category;
import org.helpdrive.helpdrive.model.Event;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventRepository extends MongoRepository<Event, String> {
    List<Event> findEventsByCategoryAndLocationNear(Category category, Point p, Distance d);

    List<Event> findByLocationNear(Point p, Distance d);
}
