package org.helpdrive.helpdrive.repo;

import org.helpdrive.helpdrive.model.Category;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends MongoRepository<Category, String> {
}

