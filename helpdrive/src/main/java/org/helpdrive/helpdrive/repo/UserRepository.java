package org.helpdrive.helpdrive.repo;

import org.helpdrive.helpdrive.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface UserRepository extends MongoRepository<User, String> {

    List<User> findByFirstName(String firstName);
}
