package org.helpdrive.helpdrive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelpdriveApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelpdriveApplication.class, args);
	}
}
