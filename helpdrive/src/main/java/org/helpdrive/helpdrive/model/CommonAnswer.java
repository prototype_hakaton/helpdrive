package org.helpdrive.helpdrive.model;


import com.fasterxml.jackson.annotation.JsonValue;

public enum CommonAnswer {
    APPROWED("APPROVED"),
    REGECT("REGECT"),
    NOT_CHOSEN_YET("NOT CHOSEN YET"),
    EVENT_NOT_FOUND("EVENT NOT FOUND"),
    USER_NOT_FOUND("USER_NOT_FOUND"),
    UNSUPPORTED_EVENT_STATE("UNSUPPORTED_EVENT_STATE");


    private final String answer;

    CommonAnswer(String answer) {
        this.answer = answer;
    }

    @JsonValue
    public String getAnswer() {
        return answer;
    }
}
