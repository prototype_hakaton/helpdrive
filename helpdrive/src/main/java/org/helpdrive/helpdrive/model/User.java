package org.helpdrive.helpdrive.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Document(collection = "User")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "firstName",
        "lastName",
        "categories",
        "location",
        "searchRadius"
})
public class User implements Serializable {

    @JsonProperty("id")
    private String id;

    @JsonProperty("firstName")
    private String firstName;

    @JsonProperty("lastName")
    private String lastName;

    @JsonProperty("categories")
    private List<Category> categories;

    @JsonProperty("location")
    private GeoJsonPoint location;

    @JsonProperty("searchRadius")
    private double searchRadius;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Category> getCategories() {
        return Category.getMockList();
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public GeoJsonPoint getLocation() {
        return location;
    }

    public void setLocation(GeoJsonPoint location) {
        this.location = location;
    }

    public double getSearchRadius() {
        return searchRadius;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        if (Double.compare(user.getSearchRadius(), getSearchRadius()) != 0) return false;
        if (getFirstName() != null ? !getFirstName().equals(user.getFirstName()) : user.getFirstName() != null)
            return false;
        if (getLastName() != null ? !getLastName().equals(user.getLastName()) : user.getLastName() != null)
            return false;
        if (getCategories() != null ? !getCategories().equals(user.getCategories()) : user.getCategories() != null)
            return false;
        return getLocation() != null ? getLocation().equals(user.getLocation()) : user.getLocation() == null;
    }

    public void setSearchRadius(double searchRadius) {
        this.searchRadius = searchRadius;
    }

    public void setHashId() {
        int result;
        long temp;
        result = getFirstName() != null ? getFirstName().hashCode() : 0;
//        result = 31 * result + (getLastName() != null ? getLastName().hashCode() : 0);
//        result = 31 * result + (getCategories() != null ? getCategories().hashCode() : 0);
        result = 31 * result + (getLocation() != null ? getLocation().hashCode() : 0);
//        temp = Double.doubleToLongBits(getSearchRadius());
//        result = 31 * result + (int) (temp ^ (temp >>> 32));
        setId( String.valueOf(result+new Date().getTime() % 1000));
    }
}
