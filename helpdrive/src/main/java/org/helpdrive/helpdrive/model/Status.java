package org.helpdrive.helpdrive.model;


import com.fasterxml.jackson.annotation.JsonValue;

public enum Status {
    OPEN("open"),
    SUBSCRIBE("subscribe"),
    APPROVED("approved"),
    IN_ACTION("in action"),
    CLOSE("close");


    private final String statusName;

    Status(String statusName) {
        this.statusName = statusName;
    }

    @JsonValue
    public String getStatusName() {
        return statusName;
    }
}
