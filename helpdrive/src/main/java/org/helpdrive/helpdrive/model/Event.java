package org.helpdrive.helpdrive.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexType;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Document(collection = "Event")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "category",
        "location",
        "userOwner",
        "userExecutor",
        "status",
        "price",
        "description"
})
public class Event {

    @JsonProperty("id")
    String id;

    @JsonProperty("category")
    Category category;

    @JsonProperty("location")
    @GeoSpatialIndexed(type = GeoSpatialIndexType.GEO_2DSPHERE)
//    @JsonDeserialize(using = GeoJsonDeserializer.class)
    GeoJsonPoint location;

    @JsonProperty("userOwner")
    User userOwner;

    @JsonProperty("userExecutor")
    Set<User> userExecutor;

    @JsonProperty(value = "status", defaultValue = "OPEN")
    Status status;

    @JsonProperty("price")
    BigDecimal price;

    @JsonProperty("description")
    String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Category getCategory() {
        return Category.getMock();
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public GeoJsonPoint getLocation() {
        return location;
    }

    public void setLocation(GeoJsonPoint location) {
        this.location = location;
    }

    public User getUserOwner() {
        return userOwner;
    }

    public void setUserOwner(User userOwner) {
        this.userOwner = userOwner;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setHashId() {
        int result = getCategory() != null ? getCategory().hashCode() : 0;
//        result = 31 * result + (getLocation() != null ? getLocation().hashCode() : 0);
//        result = 31 * result + (getUserOwner() != null ? getUserOwner().hashCode() : 0);
//        result = 31 * result + (getUserExecutor() != null ? getUserExecutor().hashCode() : 0);
//        result = 31 * result + (getStatus() != null ? getStatus().hashCode() : 0);
        result = 31 * result + (getPrice() != null ? getPrice().hashCode() : 0);
        result = 31 * result + (getDescription() != null ? getDescription().hashCode() : 0);
        setId(String.valueOf(result)
                + String.valueOf(new Date().getTime() % 1000)
        );
    }

    public Set<User> getUserExecutor() {
        return userExecutor;
    }

    public void setUserExecutor(Set<User> userExecutor) {
        this.userExecutor = userExecutor;
    }
}
