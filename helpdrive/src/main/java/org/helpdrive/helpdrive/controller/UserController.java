package org.helpdrive.helpdrive.controller;

import org.helpdrive.helpdrive.model.CommonAnswer;
import org.helpdrive.helpdrive.model.User;
import org.helpdrive.helpdrive.repo.EventRepository;
import org.helpdrive.helpdrive.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Metrics;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class UserController {

    private final UserRepository userRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @RequestMapping(value = "post/user", method = RequestMethod.POST)
    public ResponseEntity<User> addUser( User user) {
        user.setHashId();
        User save = userRepository.insert(user);
        if (save ==  null) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(save, HttpStatus.OK);
    }

    @RequestMapping(value = "get/user", method = RequestMethod.GET)
    public List<User> getUsers(@RequestParam(value = "firstName") String firstName) {
        return userRepository.findByFirstName(firstName);
    }

    @RequestMapping(value = "get/user/{id}", method = RequestMethod.GET)
    public Optional<User> getUserById(@PathVariable String id) {
        return userRepository.findById(id);
    }

    @RequestMapping(value = "update/userGeo", method = RequestMethod.POST)
    public ResponseEntity<Object> updateMyGeo(@RequestParam(value = "userId") String userId,
                                              @RequestParam(value = "x") Double x,
                                              @RequestParam(value = "y") Double y){
        Optional<User> userOp = userRepository.findById(userId);
        if (!userOp.isPresent()) {
                return new ResponseEntity<>(CommonAnswer.USER_NOT_FOUND, HttpStatus.OK);
            }
        GeoJsonPoint point = new GeoJsonPoint(x, y);
        User user = userOp.get();
        user.setLocation(point);
        userRepository.save(user);
        return new ResponseEntity<>(eventRepository.findByLocationNear(new Point(point.getX(), point.getY()), new Distance(user.getSearchRadius(), Metrics.KILOMETERS)), HttpStatus.OK);
    }
}
