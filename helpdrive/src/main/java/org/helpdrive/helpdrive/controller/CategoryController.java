package org.helpdrive.helpdrive.controller;

import org.helpdrive.helpdrive.model.Category;
import org.helpdrive.helpdrive.repo.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CategoryController {

    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryController(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @RequestMapping(value = "/post/category", method = RequestMethod.POST)
    public ResponseEntity<List<Category>> postNewCategory(@RequestBody List<Category> categoryList) {
        categoryList.forEach(Category::setHashId);
        List<Category> save = categoryRepository.insert(categoryList);
        if (save == null) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(save, HttpStatus.OK);
    }

    @RequestMapping(value = "/get/category", method = RequestMethod.GET)
    public List<Category> getCategories() {
        return categoryRepository.findAll();
    }

}

