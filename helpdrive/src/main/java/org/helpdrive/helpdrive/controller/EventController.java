package org.helpdrive.helpdrive.controller;

import org.helpdrive.helpdrive.model.CommonAnswer;
import org.helpdrive.helpdrive.model.Event;
import org.helpdrive.helpdrive.model.Status;
import org.helpdrive.helpdrive.model.User;
import org.helpdrive.helpdrive.repo.EventRepository;
import org.helpdrive.helpdrive.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class EventController {


    private final EventRepository eventRepository;
    private final UserRepository userRepository;

    @Autowired
    public EventController(EventRepository eventRepository, UserRepository userRepository) {
        this.eventRepository = eventRepository;
        this.userRepository = userRepository;
    }

    @RequestMapping(value = "/post/event",
            method = RequestMethod.POST)
    ResponseEntity<Event> postEventCategory(@RequestBody  Event event) {
        event.setHashId();
        event.setStatus(Status.OPEN);
        Event save = eventRepository.insert(event);
        if (save == null) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(save, HttpStatus.OK);
    }

    @RequestMapping(value = "/get/eventStatus/{id}", method = RequestMethod.GET)
    ResponseEntity<Object> checkEventStatus(@PathVariable String id) {
        Optional<Event> userEvent = eventRepository.findById(id);
        if (!userEvent.isPresent()) {
            return new ResponseEntity<>(CommonAnswer.EVENT_NOT_FOUND, HttpStatus.OK);
        }
        Event event = userEvent.get();
        return new ResponseEntity<>(event.getStatus(), HttpStatus.OK);
    }

    @RequestMapping(value = "/get/eventSubscribeUser/{id}", method = RequestMethod.GET,produces = {MediaType.APPLICATION_ATOM_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<Object> getSubcscribeUser(@PathVariable String id) {
        Optional<Event> userEvent = eventRepository.findById(id);
        if (!userEvent.isPresent()) {
            return new ResponseEntity<>(CommonAnswer.EVENT_NOT_FOUND, HttpStatus.OK);
        }
        Set<User> event = userEvent.get().getUserExecutor();
        return new ResponseEntity<>(event, HttpStatus.OK);
    }

    @RequestMapping(value = "/get/approveStatusMyOffer", method = RequestMethod.GET)
    ResponseEntity<CommonAnswer> checkApproveMyOffer(@RequestParam(value = "eventId") String eventId, @RequestParam(value = "userId") String userId) {
        Optional<Event> userEvent = eventRepository.findById(eventId);
        if (!userEvent.isPresent()) {
            return new ResponseEntity<>(CommonAnswer.EVENT_NOT_FOUND, HttpStatus.OK);
        }
        Event event = userEvent.get();
        if (event.getStatus().equals(Status.SUBSCRIBE)) {
            return new ResponseEntity<>(CommonAnswer.NOT_CHOSEN_YET, HttpStatus.OK);
        }
        if (event.getUserExecutor().size() == 1 && ( (User) (event.getUserExecutor().toArray()[0])).getId().equals(userId)) {
            return new ResponseEntity<>(CommonAnswer.APPROWED, HttpStatus.OK);
        }
        return new ResponseEntity<>(CommonAnswer.REGECT, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @PostMapping(value = "/update/subscribeEvent")
    ResponseEntity<Object> subscribeEvent(@RequestParam String eventId, @RequestParam String userId) {
        Optional<Event> eventOpt = eventRepository.findById(eventId);
        if (!eventOpt.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Optional<User> userOpt = userRepository.findById(userId);
        if (!userOpt.isPresent()) {
            return new ResponseEntity<>(CommonAnswer.USER_NOT_FOUND, HttpStatus.OK);
        }

        Event event = eventOpt.get();
        if (!event.getStatus().equals(Status.SUBSCRIBE) && !event.getStatus().equals(Status.OPEN)) {
            return new ResponseEntity<>(CommonAnswer.UNSUPPORTED_EVENT_STATE, HttpStatus.OK);
        }
        if (event.getStatus().equals(Status.OPEN)) {
            event.setStatus(Status.SUBSCRIBE);
        }
        Set<User> userExecutors = null;
        if (event.getUserExecutor() != null) {
            userExecutors = event.getUserExecutor();
        } else {
            userExecutors = new HashSet<>();
        }
        User user = userOpt.get();
        if (!userExecutors.contains(user)) {
            userExecutors.add(user);
        }

        User userOwner = event.getUserOwner();
        userOwner = userRepository.findById(userOwner.getId()).get();
        event.setUserOwner(userOwner);
        //у нас тут щас только id, получаем полную инфу

        event.setUserExecutor(userExecutors);
        eventRepository.save(event);
        return new ResponseEntity<>(event, HttpStatus.OK);
    }

    @GetMapping(value = "/update/closeEvent/{id}")
    ResponseEntity<Object> closeEvent(@PathVariable String id) {
        Optional<Event> eventOp = eventRepository.findById(id);
        if (eventOp.isPresent()) {
            Event event = eventOp.get();
            event.setStatus(Status.CLOSE);
            eventRepository.save(event);
            return ResponseEntity.ok(event);
        } else {
            return new ResponseEntity<>(CommonAnswer.EVENT_NOT_FOUND, HttpStatus.OK);
        }
    }

    //TODO Поменять в user id на string
    @RequestMapping(value = "/post/event/approve", method = RequestMethod.POST)
    ResponseEntity<Event> approveHelper(@RequestParam(value = "eventId") String eventId,
                                        @RequestParam(value = "userId") String userId) {
        Optional<Event> eventOp = eventRepository.findById(eventId);
        Optional<User> userOp = userRepository.findById(userId);
        if (eventOp.isPresent() && userOp.isPresent()) {
            Event event = eventOp.get();
            Set<User> set = new HashSet<>();
            set.add(userOp.get());
            event.setUserExecutor(set);
            event.setStatus(Status.APPROVED);
            eventRepository.save(event);
            return ResponseEntity.ok(event);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}