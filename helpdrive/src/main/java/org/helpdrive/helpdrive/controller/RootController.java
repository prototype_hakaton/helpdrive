package org.helpdrive.helpdrive.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

/**
 * Created by skipy on 07.04.18.
 */
@Controller
public class RootController {

    @GetMapping(value = "/")
    public ModelAndView index(Model model, Principal principal) {
        ModelAndView modelAndView = new ModelAndView("index.html");
        return modelAndView;
    }
}
