/**
 * Created by navi9 on 07.04.2018.
 */
var platform = new H.service.Platform({
    'app_id': '1w1H0RRl0GVQp66dGaG9',
    'app_code': 'LZtQX5Jkn2faGM0WMwVH6g'
});

var customerMarkers = new Array(0);
var currentPosition;
var currentUser;
var eventId;
var subscribeScheduleId;

// Obtain the default map types from the platform object
var maptypes = platform.createDefaultLayers();

// Instantiate (and display) a map object:
var map = new H.Map(
    document.getElementById('mapContainer'),
    maptypes.normal.map,
    {
        zoom: 12,
        center: {lng: 13.4, lat: 52.51}
    });
var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

var ui = H.ui.UI.createDefault(map, maptypes);


map.addEventListener('mapviewchangeend', function () {
});

$(document).ready(function () {
        document.getElementById("helpForm").style.display = 'none';
    defineCurrentUser();
        document.getElementById("solveProblem").onclick(function () {
            $.ajax({
                type: "GET",
                url: "../update/closeEvent/" + eventId,
                success: function (response) {
                    document.getElementById("solveProblem").style.display = 'none';
                },
                dataType: "json"
            })
        }
    );
}
);

    function defineCurrentUser() {
        $.ajax({
            type: "GET",
            url: "../get/user/" + getQueryParam("id"),
            // data: "data",
            success: function (response) {
                currentUser = response;
                currentPosition = {lat: currentUser.location.x + 0.02, lng: currentUser.location.y + 0.02};
                eventId = createEvent(currentUser.id);
                defineCurrentPosition();
                subscribeScheduleId = setInterval(searchSubscribers, 1000);

                $.ajax({
                    type: "POST",
                    url: "../update/userGeo",
                    data: {"userId": currentUser.id, "x": currentPosition.lat, "y": currentPosition.lng},
                    success: function (response) {
                        console.log("update convert")
                    },
                    dataType: "json",
                });
            },
            dataType: "json"
        })
    }

    function defineCurrentPosition() {
        navigator.geolocation.getCurrentPosition(function (position) {
            currentPosition = {lat: position.coords.latitude + 0.02, lng: position.coords.longitude + 0.01};
            map.setCenter(currentPosition);
            var svgMarkup = YOU_MARKUP;
            var icon = new H.map.DomIcon(svgMarkup),
                marker = new H.map.DomMarker(currentPosition, {icon: icon});
            map.addObject(marker);
        })
    }

    function stopInterval() {
        clearInterval(subscribeScheduleId);
    }

    function searchSubscribers() {
        $.ajax({
            type: "GET",
            url: "/get/eventSubscribeUser/" + eventId,
            success: function (response) {
                if (customerMarkers.length > 0) {
                    map.removeObjects(customerMarkers);
                    customerMarkers.splice(0, customerMarkers.length);
                }

                for (index = 0; index < response.length; ++index) {
                    cords = {lat: response[index].location.x, lng: response[index].location.y};
                    var icon = new H.map.DomIcon(HELPER_MARKUP),
                        marker = new H.map.DomMarker(cords, {icon: icon});
                    marker.setData("data", response[index]);
                    marker.myData = response[index];
                    marker.eventId = eventId;
                    map.addObject(marker);
                    customerMarkers.push(marker);
                    addSubscriberBubble(marker);
                }
            },
            dataType: "json",
        });
    }

    function setEvent(id) {
        eventId = id;
    }

