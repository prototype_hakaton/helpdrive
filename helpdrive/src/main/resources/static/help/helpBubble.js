function addSubscriberBubble(marker) {
    marker.addEventListener('tap', function(evt) {
        console.log(evt.target.myData.id);
        var bubble = new H.ui.InfoBubble(evt.target.getPosition(), {
            content: '<div style="font-size: 9pt">'
            + '<i>First name: </i>' + evt.target.myData.firstName + '<br>'
            + '<i>Last name: </i>' + evt.target.myData.lastName + '<br>'
            + '<a href="#" id="approveSubscriber" onclick="approve(' + evt.target.eventId
            + ',' + evt.target.myData.id + ')"><b>Approve</b></a>'
            + '</div>'
        });
         ui.addBubble(bubble);
    }, false);
}

function approve(eventId, executorId) {
    $.ajax({
        type: "POST",
        url: "/post/event/approve",
        data: {"eventId": eventId, "userId": executorId},
        success: function (response) {
            document.getElementById("helpUserId").value = response.userExecutor[0].id;
            document.getElementById("helpName").value = response.userExecutor[0].firstName + " " + response.userExecutor[0].lastName
            document.getElementById("helpForm").style.display = 'inline';
            stopInterval();

        },
        dataType: "json"
    })
}