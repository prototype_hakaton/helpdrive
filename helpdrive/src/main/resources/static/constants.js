/**
 * Created by navi9 on 07.04.2018.
 */
var YOU_MARKUP = '<svg height="30" width="30" xmlns="http://www.w3.org/2000/svg"> <circle cx="15" cy="15" r="14" stroke="black" stroke-width="1" fill="red" /><text x="15" y="19" font-size="10pt" font-family="Bradley Hand, cursive" font-weight="bold" text-anchor="middle" fill="black">You</text></svg>';
var HELP_MARKUP = '<svg height="30" width="30"  xmlns="http://www.w3.org/2000/svg"><circle cx="15" cy="15" r="14" stroke="black" stroke-width="1" fill="yellow" /><text x="15" y="19" font-size="8pt" font-family="Bradley Hand, cursive" font-weight="bold" text-anchor="middle" fill="black">Help!</text></svg>';
var HELPER_MARKUP = '<svg height="30" width="30"  xmlns="http://www.w3.org/2000/svg"><circle cx="15" cy="15" r="14" stroke="black" stroke-width="1" fill="green" /><text x="15" y="19" font-size="8pt" font-family="Bradley Hand, cursive" font-weight="bold" text-anchor="middle" fill="black">Helper</text></svg>';