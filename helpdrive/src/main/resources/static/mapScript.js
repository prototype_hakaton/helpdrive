/**
 * Created by navi9 on 07.04.2018.
 */
var platform = new H.service.Platform({
    'app_id': '1w1H0RRl0GVQp66dGaG9',
    'app_code': 'LZtQX5Jkn2faGM0WMwVH6g'
});

var customerMarkers = new Array(0);
var currentPosition;
var currentUser;

// Obtain the default map types from the platform object
var maptypes = platform.createDefaultLayers();

// Instantiate (and display) a map object:
var map = new H.Map(
    document.getElementById('mapContainer'),
    maptypes.normal.map,
    {
        zoom: 12,
        center: { lng: 13.4, lat: 52.51 }
    });
var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));


map.addEventListener('mapviewchangeend', function () {
});

var ui = H.ui.UI.createDefault(map, maptypes);


$(document).ready(function() {
    // run the first time; all subsequent calls will take care of themselves
    defineCurrentPosition();
    clear();
    $('#helpButton').click(function() {
        location.href = "/help/help.html?id=" + currentUser.id;
    })
    animateWaitingStatus();
    document.getElementById("searchForm").style.display = 'none';
});

function clear() {
    document.getElementById("searchEventId").value = '';
    document.getElementById("searchDescription").value = '';
    document.getElementById("searchPrice").value = '';
    document.getElementById("searchUserName").value = '';
}


function defineCurrentPosition() {
    navigator.geolocation.getCurrentPosition(function (position) {
        currentPosition = {lat: position.coords.latitude, lng: position.coords.longitude};
        map.setCenter(currentPosition);
        var svgMarkup = YOU_MARKUP;
        var icon = new H.map.DomIcon(svgMarkup),
        marker = new H.map.DomMarker(currentPosition, {icon: icon});
        map.addObject(marker);

        createUser();

    })
}

function createUser() {
    $.ajax({
        type: "POST",
        url: "post/user",
        data: {"firstName": "Anton", "lastName": "Ivanov", "searchRadius": 100},
        success: function (response) {
            currentUser = response;
            searchConsumers();
        },
        dataType: "json"
    })
}

function searchConsumers() {
        $.ajax({
            type: "POST",
            url: "update/userGeo",
            data: {"userId": currentUser.id, "x": currentPosition.lat, "y": currentPosition.lng},
            success: function (response) {
                if (customerMarkers.length > 0) {
                    map.removeObjects(customerMarkers);
                    customerMarkers.splice(0, customerMarkers.length);
                }

                for (index = 0; index < response.length; ++index) {
                    cords = {lat: response[index].location.x, lng: response[index].location.y};
                    var icon = new H.map.DomIcon(HELP_MARKUP),
                        marker = new H.map.DomMarker(cords, {icon: icon});
                    marker.setData("data", response[index]);
                    marker.myData = response[index];
                    map.addObject(marker);
                    customerMarkers.push(marker);
                    addBubble(marker);
                }
            },
            dataType: "json",
        });
        setTimeout(searchConsumers, 1000);
}

function addRouteShapeToMap(route){
    var lineString = new H.geo.LineString(),
        routeShape = route.shape,
        polyline;

    routeShape.forEach(function(point) {
        var parts = point.split(',');
        lineString.pushLatLngAlt(parts[0], parts[1]);
    });

    polyline = new H.map.Polyline(lineString, {
        style: {
            lineWidth: 4,
            strokeColor: 'rgba(0, 128, 255, 0.7)'
        }
    });
    // Add the polyline to the map
    map.addObject(polyline);
    // And zoom to its bounding rectangle
    map.setViewBounds(polyline.getBounds(), true);
}


/**
 * Creates a series of H.map.Marker points from the route and adds them to the map.
 * @param {Object} route  A route as received from the H.service.RoutingService
 */
function addManueversToMap(route){
    var svgMarkup = '<svg width="18" height="18" ' +
            'xmlns="http://www.w3.org/2000/svg">' +
            '<circle cx="8" cy="8" r="8" ' +
            'fill="#1b468d" stroke="white" stroke-width="1"  />' +
            '</svg>',
        dotIcon = new H.map.Icon(svgMarkup, {anchor: {x:8, y:8}}),
        group = new  H.map.Group(),
        i,
        j;

    // Add a marker for each maneuver
    for (i = 0;  i < route.leg.length; i += 1) {
        for (j = 0;  j < route.leg[i].maneuver.length; j += 1) {
            // Get the next maneuver.
            maneuver = route.leg[i].maneuver[j];
            // Add a marker to the maneuvers group
            var marker =  new H.map.Marker({
                    lat: maneuver.position.latitude,
                    lng: maneuver.position.longitude} ,
                {icon: dotIcon});
            marker.instruction = maneuver.instruction;
            group.addObject(marker);
        }
    }

    group.addEventListener('tap', function (evt) {
        map.setCenter(evt.target.getPosition());
        openBubble(
            evt.target.getPosition(), evt.target.instruction);
    }, false);

    // Add the maneuvers group to the map
    map.addObject(group);
}


/**
 * Creates a series of H.map.Marker points from the route and adds them to the map.
 * @param {Object} route  A route as received from the H.service.RoutingService
 */
function addWaypointsToPanel(waypoints){



    var nodeH3 = document.createElement('h3'),
        waypointLabels = [],
        i;


    for (i = 0;  i < waypoints.length; i += 1) {
        waypointLabels.push(waypoints[i].label)
    }

    nodeH3.textContent = waypointLabels.join(' - ');

    routeInstructionsContainer.innerHTML = '';
    routeInstructionsContainer.appendChild(nodeH3);
}

/**
 * Creates a series of H.map.Marker points from the route and adds them to the map.
 * @param {Object} route  A route as received from the H.service.RoutingService
 */
function addSummaryToPanel(summary){
    var summaryDiv = document.createElement('div'),
        content = '';
    content += '<b>Total distance</b>: ' + summary.distance  + 'm. <br/>';
    content += '<b>Travel Time</b>: ' + summary.travelTime.toMMSS() + ' (in current traffic)';


    summaryDiv.style.fontSize = 'small';
    summaryDiv.style.marginLeft ='5%';
    summaryDiv.style.marginRight ='5%';
    summaryDiv.innerHTML = content;
    routeInstructionsContainer.appendChild(summaryDiv);
}

/**
 * Creates a series of H.map.Marker points from the route and adds them to the map.
 * @param {Object} route  A route as received from the H.service.RoutingService
 */
function addManueversToPanel(route){



    var nodeOL = document.createElement('ol'),
        i,
        j;

    nodeOL.style.fontSize = 'small';
    nodeOL.style.marginLeft ='5%';
    nodeOL.style.marginRight ='5%';
    nodeOL.className = 'directions';

    // Add a marker for each maneuver
    for (i = 0;  i < route.leg.length; i += 1) {
        for (j = 0;  j < route.leg[i].maneuver.length; j += 1) {
            // Get the next maneuver.
            maneuver = route.leg[i].maneuver[j];

            var li = document.createElement('li'),
                spanArrow = document.createElement('span'),
                spanInstruction = document.createElement('span');

            spanArrow.className = 'arrow '  + maneuver.action;
            spanInstruction.innerHTML = maneuver.instruction;
            li.appendChild(spanArrow);
            li.appendChild(spanInstruction);

            nodeOL.appendChild(li);
        }
    }

    routeInstructionsContainer.appendChild(nodeOL);
}


function calculateRouteFromAtoB (pointA, pointB) {
    var router = platform.getRoutingService(),
        routeRequestParams = {
            mode: 'fastest;car',
            representation: 'display',
            routeattributes : 'waypoints,summary,shape,legs',
            maneuverattributes: 'direction,action',
            waypoint0: pointA.x + ',' + pointA.y,
            waypoint1: pointB.x + ',' + pointB.y
        };


    router.calculateRoute(
        routeRequestParams,
        onSuccess,
        onError
    );
}
/**
 * This function will be called once the Routing REST API provides a response
 * @param  {Object} result          A JSONP object representing the calculated route
 *
 * see: http://developer.here.com/rest-apis/documentation/routing/topics/resource-type-calculate-route.html
 */
function onSuccess(result) {
    var route = result.response.route[0];
    /*
     * The styling of the route response on the map is entirely under the developer's control.
     * A representitive styling can be found the full JS + HTML code of this example
     * in the functions below:
     */
    addRouteShapeToMap(route);
    addManueversToMap(route);

    addWaypointsToPanel(route.waypoint);
    addManueversToPanel(route);
    addSummaryToPanel(route.summary);
    // ... etc.
}

/**
 * This function will be called if a communication error occurs during the JSON-P request
 * @param  {Object} error  The error message received.
 */
function onError(error) {
    alert('Ooops!');
}


