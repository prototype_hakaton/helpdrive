/**
 * Created by navi9 on 07.04.2018.
 */
var animateCounter = 0;
var isStatusAnimating = false;

function addBubble(marker) {
    marker.addEventListener('tap', function (evt) {
        var bubble =  new H.ui.InfoBubble(evt.target.getPosition(), {
            // read custom data
            content: '<div style="font-size: 7pt; background: #c7d0d1;  border-radius: 12px"; font-family: Bradley Hand, cursive;>'
                + '<table><tr>'
                +'<td>'
                +'<i style="color: #c7d0d1">___________________ </i><br>'
                + '<i style="font-size: 11px"><b>Name: </b></i>' + '<b style= "font-family: Bradley Hand, cursive">' +evt.target.myData.userOwner.firstName +' ' + evt.target.myData.userOwner.lastName + '</b><br>'
            + '<i style="font-size: 11px"><b>Description: </b></i>'+ '<b style= "font-family: Bradley Hand, cursive">' + evt.target.myData.description + '</b>'
            + '<br> <i style="font-size: 11px"><b>Price: </b></i>'+ '<b style= "font-family: Bradley Hand, cursive">' + evt.target.myData.price + '</b><br><br>'
            + '<a href="#" color="red" id="helpRequest" onclick="subscr('+evt.target.myData.id +')"><b>HELP!</b></a>'
                + '</td>'
                + '<td><img src="man.png" width="60" height="85"></td>'
            + '</tr></table>'
             + '</div> '
        });
        ui.addBubble(bubble);
    }, false);
}

function subscr(eventId) {
    $.ajax({
        type: "POST",
        url: "update/subscribeEvent",
        data: {"eventId": eventId, "userId": currentUser.id},
        success: function (response) {
            document.getElementById("searchForm").style.display = 'inline';

            document.getElementById("searchEventId").value = response.id;
            document.getElementById("searchDescription").value = response.description;
            document.getElementById("searchPrice").value = response.price;
            document.getElementById("searchUserName").value = response.userOwner.firstName + " " + response.userOwner.lastName;
            checkApproveMyOffer(response);
        },
        dataType: "json"
    })
}
function checkApproveMyOffer(event) {
    ///get/approveStatusMyOffer/{id}
    $.ajax({
        type: "GET",
        url: "../get/approveStatusMyOffer",
        data: {userId: currentUser.id,
            eventId: event.id},
        success: function (response) {
            switch (response) {
                case "APPROVED" :
                    console.log("route to " + event.location);
                    var curPosPair = {
                        x :currentPosition.lat,
                        y : currentPosition.lng
                    };
                    calculateRouteFromAtoB(curPosPair, event.location)
                    isStatusAnimating = false;
                    document.getElementById("statusText").textContent = "APPROVED!";
                    setTimeout(function () {
                        document.getElementById("statusText").style.display = 'none';
                    }, 1500);
                    break;
                case "REGECT" :
                    isStatusAnimating = false;
                    document.getElementById("statusText").textContent = "REJECTED!";
                    setTimeout(function () {
                        document.getElementById("statusText").style.display = 'none';
                    }, 1500);
                    break;
                default :
                    console.log(response);
                    setTimeout(function(){checkApproveMyOffer(event)}, 1200);
                    isStatusAnimating = true;
                    break;
            }
        },
        dataType: "json"
    });
}


function animateWaitingStatus() {
    if (isStatusAnimating) {
        var statusText = document.getElementById("statusText");
        statusText.textContent = "Waiting for approve"
        var dots;
        if (animateCounter % 4 == 0) dots = "";
        if (animateCounter % 4 == 1) dots = ".";
        if (animateCounter % 4 == 2) dots = "..";
        if (animateCounter % 4 == 3) dots = "...";
        statusText.textContent += dots;
        animateCounter++;

    }
    setTimeout(animateWaitingStatus, 800);
}